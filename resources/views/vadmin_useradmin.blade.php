@extends('admin_template')

@section('content')
  <link rel="stylesheet" href="{{ asset("/adminside/plugins/datatables/dataTables.bootstrap.css") }}">
    <div class='row'>
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Telepon</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <td>1</td>
                <td>Loki</td>
                <td>loki@gmail.com</td>
                <td>0909090</td>
                <td>GO</td>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </div><!-- /.row -->
    <!-- jQuery 2.2.3 -->
<script src="{{ asset("/adminside/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset("/adminside/bootstrap/js/bootstrap.min.js") }}"></script>
<!-- DataTables -->
<script src="{{ asset("/adminside/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("/adminside/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
<!-- SlimScroll -->
<script src="{{ asset("/adminside/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
<!-- FastClick -->
<script src="{{ asset("/adminside/plugins/fastclick/fastclick.js") }}"></script>

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endsection