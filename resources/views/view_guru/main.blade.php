@extends('admin_template')

@section('content')
    <link rel="stylesheet" href="{{ asset("/adminside/plugins/datatables/dataTables.bootstrap.css") }}">
    <div class='row'>
        <div class='col-xs-12'>
          <div class="box">
            <div class="box-header">
              <button type="button" class="btn btn-primary btn-md">
                      <a href="{{ route('kelola_guru_tambah') }}" style="color: white">
                        <i class="fa fa-user-plus"></i>
                        Tambah Guru
                      </a>
              </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Foto</th>
                  <th>NIP</th>
                  <th>Nama Lengkap</th>
                  <th>Username</th>
                  <th>Tempat, Tanggal Lahir</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $num=1; ?>
                @foreach($guru as $key)
                <tr>
                  <td>{{ $num++ }}</td>
                  <td><img style="width:30px" src="{{ asset('images/' . $key->foto) }}" /></td>
                  <td>{{ $key->nip }}</td>
                  <td>{{ $key->nama_lengkap }}</td>
                  <td>{{ $key->username }}</td>
                  <td>{{ $key->tempat_lahir }}, {{ $key->tanggal_lahir }}</td>
                  <td>
                  @if($key->status==1)
                      <label class="label label-success">Aktif</label>
                  @else
                      <label class="label label-danger">Non-Aktif</label>
                  @endif
                  </td>
                  <td>
                    <form class="delete" method="POST" action="{{ route('kelola_guru_hapus', $key->id) }}" accept-charset="UTF-8">
                    <button type="button" class="btn btn-warning btn-xs">
                      <a  data-toggle="tooltip" title="Reset Password" style="color: white">
                        <i class="fa fa-refresh"></i>
                      </a>
                    </button>
                    <button type="button" class="btn btn-info btn-xs">
                      <a  data-toggle="tooltip" title="Detail" style="color: white">
                        <i class="fa fa-search"></i>
                      </a>
                    </button>
                    <button type="button" class="btn btn-warning btn-xs"><a href="{{ route('kelola_guru_edit',$key->id) }}" data-toggle="tooltip" title="Edit" style="color: white"><i class="fa fa-edit"></i></a></button>
                    <input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <button data-toggle="tooltip" title="Delete" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </form>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
            
        </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- jQuery 2.2.3 -->
    <script src="{{ ("/adminside/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- DataTables -->
    <script src="{{ ("/adminside/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ ("/adminside/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
    <!-- SlimScroll -->
    <script src="{{ ("/adminside/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <!-- FastClick -->
    <script src="{{ ("/adminside/plugins/fastclick/fastclick.js") }}"></script>
    <script>
    $(function () {
        $("#table1").DataTable();
    
    });
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
    </script>
@endsection