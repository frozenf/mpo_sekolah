	@extends('admin_template')

	@section('content')
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="{{ asset("/adminside/plugins/datepicker/datepicker3.css") }}">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Form</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('kelola_guru_simpan') }}" method="post" enctype="multipart/form-data" role="form">
					<div class="box-body">
						{{ csrf_field() }}
						<div class="col-md-6"> 
							<div class="form-group {{$errors->has('nip') ? ' has-error' : ''}}">
								<label for="inputNip">NIP</label>
								<input type="number" name="nip" class="form-control" id="inputNip" placeholder="Masukkan NIP">
								{!! $errors->first('nip', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group {{$errors->has('nama_lengkap') ? ' has-error' : ''}}">
								<label for="inputNama">Nama Lengkap</label>
								<input type="text" name="nama_lengkap" class="form-control" id="inputNama" placeholder="Masukkan Nama">
								{!! $errors->first('nama_lengkap', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group {{$errors->has('username') ? ' has-error' : ''}}">
								<label for="inputUsername">Username</label>
								<input type="text" name="username" class="form-control" id="inputUsername" placeholder="Masukkan Username">
								{!! $errors->first('username', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group  col-md-6 {{$errors->has('jenis_kelamin') ? ' has-error' : ''}}">
								<label for="inputGender">Jenis Kelamin</label>
								<div class="radio">
									<label>
										<input type="radio" name="jenis_kelamin" id="inputGender" value="1" checked>
										<i class="fa fa-male"></i> Laki-Laki
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="jenis_kelamin" id="inputGender" value="0">
										<i class="fa fa-female"></i> Perempuan
									</label>
								</div>
								{!! $errors->first('jenis_kelamin', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group col-md-6 {{$errors->has('status') ? ' has-error' : ''}}">
								<label for="inputStatus">Status</label>
								<div class="radio">
									<label>
										<input type="radio" name="status" id="inputStatus" value="1" checked>
										Aktif
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="status" id="inputStatus" value="0">
										Non-Aktif
									</label>
									{!! $errors->first('status', '<p class="help-block">:message</p>') !!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group {{$errors->has('tempat_lahir') ? ' has-error' : ''}}">
								<label for="inputTempatLahir">Tempat Lahir</label>
								<input type="text" name="tempat_lahir" class="form-control" id="inputTempatLahir" placeholder="Masukkan Tempat Lahir">
								{!! $errors->first('tempat_lahir', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group {{$errors->has('tanggal_lahir') ? ' has-error' : ''}}">
								<label>Tanggal Lahir</label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="tanggal_lahir" class="form-control pull-right" id="datepicker">
								</div>
								{!! $errors->first('tanggal_lahir', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group {{$errors->has('alamat') ? ' has-error' : ''}}">
								<label for="inputAlamat">Alamat</label>
								<input type="text" name="alamat" class="form-control" id="inputAlamat" placeholder="Masukkan Alamat">
								{!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group {{$errors->has('foto') ? ' has-error' : ''}}" >
								<label for="inputFoto">Foto</label>
								<input type="file" name="foto" id="inputFoto" accept="image/*">
								<img src="{{ asset('images/person.png') }}" id="uploadedimage" width="100px" height="100px" />
								<p>File Size Max 250KB</p>
								<p>
									<span id="imageerror" style="font-weight: bold; color: red"></span>
								</p>
								{!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<div class="pull-right">
						<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<!-- jQuery 2.2.3 -->
	<script src="{{ asset("/adminside/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
	<!-- bootstrap datepicker -->
	<script src="{{ asset("/adminside/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
	<script>
		$(document).ready(function() {
			document.getElementById("inputFoto").onchange = function () {
				var reader = new FileReader();

				reader.onload = function (e) {
					if (e.total > 250000) {
						$('#imageerror').text('Image too large');
						$jimage = $("#inputFoto");
						$jimage.val("");
						$jimage.wrap('<form>').closest('form').get(0).reset();
						$jimage.unwrap();
						$('#uploadedimage').removeAttr('src');
						return;
					}
					$('#imageerror').text('');
					document.getElementById("uploadedimage").src = e.target.result;
				};
				reader.readAsDataURL(this.files[0]);
			};
		});
		//Date picker
    	$('#datepicker').datepicker({ format: 'yyyy-mm-dd', autoclose:true });
	</script>

	@endsection