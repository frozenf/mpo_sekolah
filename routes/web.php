<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin', ['as' => 'admin_dashboard',
	'uses' =>  'AdminDashboardController@index']);

Route::resource('admin/manage_user/admin', 'AdminController',
                ['names' =>
                    [
                        'index' => 'kelola_admin_index',
                        'create' => 'kelola_admin_tambah',
                        'store' => 'kelola_admin_simpan',
                        'edit' => 'kelola_admin_edit',
                        'update' => 'kelola_admin_update',
                        'destroy' => 'kelola_admin_hapus',
                    ]
                ]);

Route::resource('admin/manage_user/teacher', 'GuruController',
                ['names' =>
                	[
                		'index' => 'kelola_guru_index',
                		'create' => 'kelola_guru_tambah',
                		'store' => 'kelola_guru_simpan',
                		'edit' => 'kelola_guru_edit',
                		'update' => 'kelola_guru_update',
                		'destroy' => 'kelola_guru_hapus',
                	]
                ]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
