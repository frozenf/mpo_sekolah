<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = "Manage User Admin";
        $menu_user = "active";
        $menu_user_admin = "active";
        $admin = Admin::get();
        return view('view_admin.main',compact('page_title','menu_user','menu_user_admin','admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Tambahkan User Admin";
        return view('view_admin.form_add',compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = new admin();
        $admin->nama_lengkap = $request->nama_lengkap;
        $admin->email = $request->email;
        $admin->password = Hash::make('12345678');
        $admin->status = $request->status;
        $admin->telp = $request->telp;
        $admin->foto = 'person.png';
        if(Input::hasfile('foto')) {
            $file=Input::file('foto');
            $file->move(public_path().'/images/', $file->getClientOriginalName());
            $admin->foto = $file->getClientOriginalName(); 
        }
        $admin->save();
        return redirect()->route('kelola_admin_index')->with('alert-success', 'Data Berhasil Ditambah');      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
