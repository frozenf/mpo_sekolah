<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Guru;
use Illuminate\Support\Facades\Input;


class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = "Manage User Guru";
        $menu_user = "active";
        $menu_user_teacher = "active";
        $guru = Guru::get();
        return view('view_guru.main',compact('page_title','menu_user','menu_user_teacher','guru'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Tambahkan User Guru";
        return view('view_guru.form_add',compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guru = new guru();
        $guru->nama_lengkap = $request->nama_lengkap;
        $guru->username = $request->username;
        $guru->password = Hash::make('12345678');
        $guru->nip = $request->nip;
        $guru->jenis_kelamin = $request->jenis_kelamin;
        $guru->status = $request->status;
        $guru->tempat_lahir = $request->tempat_lahir;
        $guru->tanggal_lahir = $request->tanggal_lahir;
        $guru->alamat = $request->alamat;
        $guru->foto = 'person.png';
        $guru->id_admin = 1;
        if(Input::hasfile('foto')) {
            $file=Input::file('foto');
            $file->move(public_path().'/images/', $file->getClientOriginalName());
            $guru->foto = $file->getClientOriginalName(); 
        }
        $guru->save();
        return redirect()->route('kelola_guru_index')->with('alert-success', 'Data Berhasil Ditambah');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = Guru::findOrFail($id);
        $page_title = "Edit User Guru";
        return view('view_guru.form_edit',compact('guru','page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guru = Guru::findOrFail($id);
        $guru->nama_lengkap = $request->nama_lengkap;
        $guru->username = $request->username;
        $guru->nip = $request->nip;
        $guru->jenis_kelamin = $request->jenis_kelamin;
        $guru->status = $request->status;
        $guru->tempat_lahir = $request->tempat_lahir;
        $guru->tanggal_lahir = $request->tanggal_lahir;
        $guru->alamat = $request->alamat;
        $guru->id_admin = 1;
        if(Input::hasfile('foto')) {
            $file=Input::file('foto');
            $file->move(public_path().'/images/', $file->getClientOriginalName());
            $guru->foto = $file->getClientOriginalName(); 
        }
        $guru->save();
        return redirect()->route('kelola_guru_index')->with('alert-success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guru = Guru::findOrFail($id);
        $guru->delete();
        return redirect()->route('kelola_guru_index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
